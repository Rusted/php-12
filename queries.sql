use baltic_talents;

-- 1. visų darbuotojų įrašų vardą, pavardę ir gimimo datą
SELECT name, surname, birthday FROM employees;
-- 2. visiems darbuotojų įrašams
SELECT * FROM employees;
-- 3. visų darbuotojų išsilavinimą be pasikartojančių įrašų
SELECT DISTINCT education FROM employees;
-- 4. visų darbuotojų metiniam ir valandiniam atlyginimui, pavadinkite stulpelius: hourly_salary, yearly_salary
SELECT *,salary/168 hourly_salary, salary * 12 yearly_salary FROM employees
-- 5. visiem darbuotojus, kurių atlyginimas didesnis už 1000
SELECT * FROM employees WHERE salary > 1000;
-- 6. darbuotojus, kurių telefono numeris yra NULL
SELECT * FROM employees WHERE phone IS NULL;
-- 7. darbuotojus, kurių telefono numeris nėra NULL
SELECT * FROM employees WHERE phone IS NOT NULL;
-- 8. darbuotojus, į kurių education yra įrašytas Universitetas. Panaudokite LIKE
SELECT * FROM employees WHERE education like '%university%';
-- 9. Darbuotojų gimimo metus, mėnesį ir dieną atskiruose stulpeliuose
SELECT *, YEAR(birthday) birth_year, MONTH(birthday) birth_month, DAY(birthday) birth_day FROM employees;
-- 9. darbuotojus, kurie gimę 2000 arba 2005 arba 2010 metais
SELECT * FROM employees WHERE YEAR(birthday) IN (2000, 2005, 2010);
-- 10. Darbuotjų amžių
SELECT *,TIMESTAMPDIFF(YEAR, birthday, CURDATE()) age FROM employees
-- 11. Darbuotojų vardo ilgį
SELECT name, LENGTH(name) name_length FROM employees;
-- 12 pirmus 3 darbuotojus
SELECT * FROM employees LIMIT 3;